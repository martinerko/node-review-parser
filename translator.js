var express = require('express');
var config = require("./config");
var async = require("async");
var app = express();

var MsTranslator = require('mstranslator');
var MongoClient = require("mongodb").MongoClient;
var dbConnectionString = config.dbConnectionString;
var reviewsCollection = config.reviewsCollection;
var categories = {
	"Highlights": "ZVY",
	"DIY": "DIY",
	"Curly": "KUC7",
	"Double Wefted": "DEL",
	"One Piece Top": "QUI",
	"Wavy": "VLN7",
	"Ponytail": "COP",
	"Fringe": "OFIN",
	"Weft/Weave": "WEFTVL",
	"I-Tip": "KER",
	"U-Tip Pre": "KER",
	"U-Tip Bodywave": "KERVLN",
	"Inch Full Head": "C7",
	"Micro": "MIC"
};

var q = {
	//"length": "15"
	//"model": "#1"
};

var client = new MsTranslator({
	client_id: config.msAzure.clientID,
	client_secret: config.msAzure.clientSecret
});


function translate(review, callback) {
	client.translate({
		text: review.text,
		from: 'en',
		to: 'sk'
	}, function(err, translatedText) {

		if (err) {
			console.log(err.message);
		} else {
			console.log(review.text);
			console.log(translatedText);
			review.bingTranslation = translatedText;
		}
		callback(err);
	});
}

function translateProductReviews(item, productCallback) {
	console.log(item.product);
	resolveProductModel(item);
	resolveProductCode(item);
	console.log(item.model, item.category);
	//return callback();

	//translate each review
	async.each(item.reviews, translate, function(err) {
		if (err) {
			console.log(err.message);
		}
		productCallback(err);
	});
}

function resolveProductModel(item) {
	var model = item.product.match(/\(#.+\)/);
	if (model.length) {
		item.model = model[0].replace(/[\(\)]/g, "");
	} else {
		console.log("can not resolve model");
	}
}

function resolveProductCode(item) {
	for (var c in categories) {
		if (~item.product.indexOf(c)) {
			item.category = categories[c];
			return;
		}
	}
}

app.get('/translator', function(req, res) {
	client.initialize_token(function(keys) {
		MongoClient.connect(dbConnectionString, function(err, db) {
			if (err) {
				throw err;
			}
			db.collection(reviewsCollection).find(q).toArray(function(err, data) {
				if (err) {
					throw err;
				}
				async.each(data, function(item, callback) {
					var productCallback = function(err) {
						if (err) {
							console.log(err.message);
						}

						console.log("=================")
						console.log(item);
						

						db.collection(reviewsCollection).update({
							"_id": item._id
						}, item, function(err) {
							if (err) {
								console.log(err.message);
							}
							console.log("========== moving to next product===========");
							//after saving move to next product
							callback(err);
						});
					};

					translateProductReviews(item, productCallback);
				}, function(err) {
					if (err) {
						console.log("A translation failed to process: %s", err.message);
					} else {
						console.log("all reviews has been translated");
					}
					db.close();
				});
			});
		});
	});
});

app.listen('3000');
console.log('run localhost:3000/translator');