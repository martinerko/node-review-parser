var express = require('express');
var request = require("request");
var cheerio = require("cheerio");
var mongodb = require("mongodb");
var async = require("async");
var _ = require("lodash");
var app = express();

app.get('/reviews', function(req, res){

	var MongoClient = require("mongodb").MongoClient
	var dburl = "mongodb://localhost:27017/node-parser";

	var sitemapurl = "http://www.cliphair.co.uk/sitemap.php";


	function parseReview(db, link, callback){
		
		request("http://www.cliphair.co.uk/" + link, function(error, response, html){
			if(error){
				callback(null, error);
		  	}
			
			var $ = cheerio.load(html);
			console.log("processing %s", $('h1').text());
			var reviews = [];
			$(".creviews-reviews-list > li").filter(function(){
				var review = $(this).text().match(/(\s+\w+\s+)(\w.*)/);
				review && reviews.push({name:review[1].trim(), text: review[2].trim()});
			});
			if(reviews.length){
				console.log("found %s reviews", reviews.length);
				var productInfo = $('h1').text().replace(/\/ #/g, "/").match(/(\d+)[a-zA-Z\s]+.*(#\d+(\/\d+)?).*/);
				if(productInfo){
					var q = {
						product : productInfo[0],
						length : productInfo[1],
						model : productInfo[2]
					};

					db.collection("reviews").update(q, _.extend({reviews:reviews}, q), {upsert:true}, function(err, result){
						if(err){
							console.log("error while saving review %s", err.message);
						}
						callback(null, err);
					});
				}else{
					callback();
				}
			}else{
				console.log("no review found", reviews.length);
				callback();
			}
		});
	}


	MongoClient.connect(dburl, function(err, db) {
	  	if(err){
			throw err;
	  	}

	  	//find links to all products
	  	request(sitemapurl, function(error, response, html){
			console.log("sitemap loaded");  
			if(error){
				throw error;
		  	}
			
			var $ = cheerio.load(html);
			async.each($('.sitemap_products li a'), function(a, callback){
				var link = $(a).attr('href');
				parseReview(db, link, callback);
			}, function(err){
				if(err){
					console.log("there was an error while parsing reviews: %s", err.message);
				}else{
					console.log("all reviews parsed sucessfully");
					console.log("closing db");					
					db.close();
				}
			});
		});
	});
});

app.listen('3000')
console.log('run localhost:3000/reviews');
